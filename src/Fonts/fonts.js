import { createGlobalStyle } from 'styled-components';
import HelveticaBold from './Helvetica-Bold.ttf';

export default createGlobalStyle`
    @font-face {
        font-family: 'Font Name';
        src: url(${HelveticaBold});    
    }
`;