import styled from 'styled-components'
import back from '../../images/Home-hintergrund.svg'


export const Pattern = styled.div`
    background-image: url(${back});
    height: 1000px;
    background-size: cover;
    background-repeat: no-repeat;
    display: block;
    
    
`

export const HomeDiv = styled.div`
    background: #292929;
    color: #f2f2f2;
    height: 500px;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    
`

export const Sloganbox = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin-right: 100px;
    margin-top: 700px;
    text-align: right;
    flex-direction: column;
    
`

export const Slogan = styled.h1`
    font-size: 155px;
    font-family: Helvetica;
    font-weight: bold;
    /* font-family: HelveticaBold; */
`


