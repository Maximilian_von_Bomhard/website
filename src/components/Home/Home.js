import React from 'react'
import {HomeDiv, Pattern, Slogan, Sloganbox} from './styleHome'
import GlobalFonts from '../../Fonts/fonts';

function Home() {
    return (
        <div  id="home" >
            <HomeDiv>
                <GlobalFonts/>
                <Sloganbox>
                    <Slogan>Lighting up <br/> potential.</Slogan>
                    {/* <Slogan>potential.</Slogan> */}
                </Sloganbox>
            </HomeDiv>

            <Pattern/>

            

                
           
        </div>
    )
}

export default Home
