import React, {useState} from 'react'
import { Anchor } from 'antd';
import logoLight from '../../images/logo_light_text.svg'
import logoDark from '../../images/logo_dark_text.svg'
import { NavDiv } from './styleNav';
import '../../App.css'




const { Link } = Anchor;




function Navbar() {

    const [black, setblack] = useState(false);
    const [home, setHome] = useState(true);
    const [about, setAbout] = useState(false);
    const [team, setTeam] = useState(false);
    const [projects, setProjects] = useState(false);
    const [contact, setContact] = useState(false);
    
    
      


  

    const changeBackground = () => {
        if(window.scrollY<=1100){
            setHome(true);
            setblack(false);

            setProjects(false);
            setTeam(false);
            setContact(false);
            setAbout(false);
        }
        else if(window.scrollY>1100 &&  window.scrollY<2200){
            setblack(true);
            setAbout(true);

            setHome(false);
            setProjects(false);
            setTeam(false);
            setContact(false);
        }
        else if(window.scrollY>=2200 &&  window.scrollY<4000){
            setblack(true);
            setTeam(true);

            setHome(false);
            setProjects(false);
            setContact(false);
            setAbout(false);
        }
        else if(window.scrollY>=4000 &&  window.scrollY<5200){
            setblack(false);
            setProjects(true);

            setHome(false);
            setTeam(false);
            setContact(false);
            setAbout(false);
        }
        else if(window.scrollY>=5200){
            setblack(true);
            setContact(true);

            setHome(false);
            setTeam(false);
            setProjects(false);
            setAbout(false);
        }
        
         //console.log(window.scrollY);
    }

    window.addEventListener('scroll', changeBackground);


    

    return (
        <div className="navigation">
            
            <nav className="navbar navbar-expand"> 
                <div className="container">

                        <Anchor targetOffset='50' style={{paddingTop:'20px'}}>
                            <Link href="#home" className="navbar-brand" title={ <img src={black? logoLight: logoDark} height='70px' alt='logo'/>} />
                        </Anchor>

                        <div  className="navbar-nav ml-auto">
                            
                        
                            <Anchor targetOffset='50'   style={{paddingTop:'20px'}} >
                                 <NavDiv className='navbar-nav ml-auto'>
                                   
                                    <Link href="#home" title='Home' className={home? 'nav-link active' : black? 'nav-link black' : 'nav-link white'}/>
                                    
                                    <Link  href="#about" title="About" className= { about? 'nav-link active' : black? 'nav-link black' : 'nav-link white'}/>
                                   
                                    <Link  href="#team" title="Team" className={team? 'nav-link active' : black? 'nav-link black' : 'nav-link white'} />

                                    <Link  href="#projects" title="Projects" className={projects? 'nav-link active' : black? 'nav-link black' : 'nav-link white'} />
                                   
                                    <Link  href="#contact" title="Contact" className={contact? 'nav-link active' : black? 'nav-link black' : 'nav-link white'} />
                                    
                                </NavDiv>
                            </Anchor>
                        
                            

                        </div>
                    </div>
            </nav>     
                
            
            
        </div>
    )
}

export default Navbar
