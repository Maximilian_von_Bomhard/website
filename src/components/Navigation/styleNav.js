import styled from 'styled-components'


export const NavDiv = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 160%;
    font-size: 20px;    
`

