import styled from 'styled-components'
import {Link} from 'react-scroll'

export const Nav = styled.nav`
    background: transparent;
    height: 80px;
    /* margin-top: -80px; */
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 1rem;
    position: sticky;
    top: 0;
    z-index: 10;

    @media screen and (max-width: 960px){
        transition: 0.8s all ease;
    }
`

export const NavbarContainer = styled.div`
    display: flex;
    justify-content: space-between;
    height: 80px;
    z-index: 1;
    width: 100%;
    padding-top: 20px;
    max-width: 1300px;
`

export const NavLogo = styled(Link)`
    justify-self: flex-start;
    cursor: pointer;
    display: flex;
    align-items: center;
    transition: ease 0.3s;
   
    
`

// export const MobileIcon = styled.div`
//     display: none;

//     @media screen and (max-width: 768px){
//         display: block;
//         position: absolute;
//         top: 0;
//         right:0;
//         transform: translate(-100%, 60%);
//         font-size: 1.8rem;
//         cursor: pointer;
//         color: #fff;
//     }

// `

export const NavMenu = styled.div`
    display: flex;
    align-items: center;
    list-style: none;
    text-align: center;
    justify-content: space-around;
    width: 60%;
    
    
    @media screen and (max-width: 768px){
        display: none;
    }
` 

export const NavItem = styled.li`
    height: 80px;
`


export const NavLinks = styled(Link)`
    &&&&{ 
        color: ${props => props.color};

        &.active{
            color: #FF0084;
            /* text-transform: uppercase; */
        }
        font-family: Helvetica;
        
    }
    
    display: flex;
    align-items: center;
    text-decoration: none;
    padding: 0 1rem;
    height: 100%;
    cursor: pointer;
    font-size: 20px; 
    transition: ease 0.3s; 
`
