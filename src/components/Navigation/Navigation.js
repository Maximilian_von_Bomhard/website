import React, {useEffect, useState} from 'react'
import { Nav, NavbarContainer, NavLogo, NavMenu, NavItem, NavLinks} from "./styleNavigation";
//import {FaBars} from 'react-icons/fa'
import logoLight from '../../images/logo_light_text.svg'
import logoDark from '../../images/logo_dark_text.svg'

function Navigation() {
    const [black, setblack] = useState(false);

    const changeNav = () => {
        if(window.scrollY>=1200){
            setblack(true);
        }
        else{
            setblack(false);
        }
    }

    useEffect(()=> {
        window.addEventListener('scroll', changeNav)
    }, []);


    return (
        <>
            <Nav>
                <NavbarContainer>
                    <NavLogo to='home' smooth={true} duration={500} spy={true} exact='true' offset={-80}><img src={black? logoLight: logoDark} height='70px' alt='logo'/></NavLogo>
                    {/* <MobileIcon>
                        <FaBars/>
                    </MobileIcon> */}
                    <NavMenu>
                        <NavItem>
                            <NavLinks to='home' color={black? 'black': 'white'} smooth={true} duration={500} spy={true} exact='true' offset={-80}>Home</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to='about' color={black? 'black': 'white'} smooth={true} duration={500} spy={true} exact='true' offset={-80}>About</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to='team' color={black? 'black': 'white'} smooth={true} duration={500} spy={true} exact='true' offset={-80}>Team</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to='projects' color={black? 'black': 'white'} smooth={true} duration={500} spy={true} exact='true' offset={-80}>Projects</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to='contact' color={black? 'black': 'white'} smooth={true} duration={500} spy={true} exact='true' offset={-80}>Contact</NavLinks>
                        </NavItem>
                    </NavMenu>
                </NavbarContainer>
            </Nav>
        </>
    )
}

export default Navigation
