import React from 'react'
import image from '../../images/warm.jpg'
import {Text,ImageBox, Container, Image, Textbox, Name} from './styleTeam'


function Worker() {
    return (
        <Container>

            <ImageBox>
                <Image src={image}  height='300px' width='100%'  />
            </ImageBox>

            <Textbox>
                <Text>
                    <Name>
                        Maximilian <br/>von Bomhard
                    </Name>
                        At vero eos et accusam et justo duo 
                        dolores et ea rebum. Stet clita kasd 
                        gubergren, no sea takimata sanctus est 
                        sit amet. 
                        Lorem ipsum dolor sit amet, consetetur 
                        sadipscing elitr, sed diam nonumy 
                        eirmod tempor invidunt ut labore et 
                        dolore magna. Dolor sit amet,
                </Text>
            </Textbox>
        </Container>
    )
}

export default Worker
