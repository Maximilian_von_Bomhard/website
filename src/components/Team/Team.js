import React, {useEffect} from 'react'
import { WorkerBox, Box } from './styleTeam'
import Worker from './Worker'
import Aos from 'aos'
import 'aos/dist/aos.css'

function Team() {

    useEffect(() => {
        Aos.init({duration: 1500});
    }, []);

    return (
        <div id="team">
            <Box >
                <WorkerBox data-aos='fade-up'>
                    <Worker/>
                    <Worker/>
                    <Worker/>
                    <Worker/>
                    <Worker/>
                    <Worker/>
                </WorkerBox>
            </Box>
        </div>
    )
}

export default Team
