import styled from 'styled-components'


export const Box = styled.div`
    background: linear-gradient(to bottom, #f2f2f2, white);
    transform: sKewY(-6deg);
    display: flex;
    justify-content: center;
    z-index: 1;
    
    
`

export const WorkerBox = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    width: 80%;
    padding-top: 90px;
    
`

export const Container = styled.div`
    margin: 30px;
    width: 400px;
    height: 700px;
    
    
    
    
`
export const Image = styled.img`
    transform: sKewY(6deg);
    clip-path: polygon(0 42px, 100% 0%, 100% 88%, 0 100%);
`
export const ImageBox = styled.div`
`

export const Textbox = styled.div`
    background: #f2f2f2;
    margin-top: -20px;

`


export const Text = styled.div`
    transform: sKewY(6deg);
    text-align: left;
    padding-top: 50px;
    width: 80%;
    padding-bottom: 60px;
    margin-left: 30px;
    font-size: 20px;
    font-family: Calibri;
`
export const Name = styled.div`
    font-family: Helvetica;
    font-size: 35px;
    margin-bottom: 10px;
   

`

