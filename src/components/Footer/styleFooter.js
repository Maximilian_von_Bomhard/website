import styled from "styled-components"

export const Container = styled.div`
    background-color: #f2f2f2;
    height: 700px;
    width:100%;
    clip-path: polygon(0 400px, 100% 20%, 100% 100%, 0 100%);
    display: flex;
    justify-content: flex-end;
    flex-direction: column;
`

export const Box = styled.div`

    display: flex;
    width: 100%;
    align-items: flex-end;
    justify-content: space-between;

`

export const Icons = styled.div`
    display: flex;
    justify-content: space-evenly;
    width: 10%;
    margin-bottom: 150px;
    //margin-top: 500px;
    
`

export const Left = styled.div`
    display: flex;
    justify-content: space-evenly;
    width: 60%;
    flex-wrap: wrap;
    margin-bottom: 150px;
    
    
    
`

export const Button = styled.button`
    border: unset;
    text-decoration: none;
    background-color: #f2f2f2;
    font-family: Helvetica;
    font-size: 15px;
    cursor: pointer;


`

export const Datenschutz = styled.div`
    
        background-color: #f2f2f2;
        height: 0px;
        overflow: hidden;
        transition: height ease 0.3s;
    

`

export const Impressum = styled.div`
    
    background-color: #f2f2f2;
    height: 0px;
    overflow: hidden;
    
`