
import React, {useState, useRef} from "react";
import { Box, Container, Icons, Left, Button, Datenschutz, Impressum } from "./styleFooter";
import { SocialIcon } from 'react-social-icons';




function Footer() {

  const [dataOpen, setDataOpen] = useState(false);
  const [imprOpen, setImprOpen] = useState(false);

  const DataRef = useRef();
  const ImprRef = useRef();

  return (
    <div>
      <Container>
        <Box>
          <Left>
            <div>
              © 2021 Titanom Holding, all rights reserved
            </div>

            <div>
              <Button onClick={() => {setDataOpen(!dataOpen)}}>Datenschutz</Button>
            </div>

            <div>
              <Button onClick={() => {setImprOpen(!imprOpen)}}>Impressum</Button>
            </div>
          </Left>

          <Icons>
            <div>
              <SocialIcon network="twitter" bgColor="#292929" />
            </div>

            <div>
              <SocialIcon network="instagram" bgColor="#292929" />
            </div>

            <div>
              <SocialIcon network="facebook" bgColor="#292929" />
            </div>

          </Icons>

        </Box>

        
      </Container>

    
      <Datenschutz ref={DataRef}   style={dataOpen? {height: DataRef.current.scrollHeight + 'px'}: {height: '0px'}}>
        <div>
          <h1>
            Datenschutz
          </h1>

          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore 
            et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   

            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   

            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.   

            Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   

            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   

            At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur

          </p>
        </div>
      </Datenschutz>
    

    
      <Impressum ref= {ImprRef} style={imprOpen? {height: ImprRef.current.scrollHeight + 'px'}: {height: '0px'}}>
        <h1>Impressum</h1>
      </Impressum>
    

      
  </div>
             
  );
}

export default Footer;