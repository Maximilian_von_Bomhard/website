import React, {useEffect} from 'react'
import { Pattern,Box, Column1, Column2, TitleBox, Left, Title, Text} from './styleProjects'
import Docuware from '../../images/DocuwareLogo.svg'
import Digi from '../../images/Digi1.svg'
import Aos from 'aos'
import 'aos/dist/aos.css'

function Projects() {

    useEffect(() => {
        Aos.init({duration: 1500});
    }, []);

    return (
       
        <Pattern id="projects">
            <Box data-aos='fade-up'>
                <Left>
                    <TitleBox>
                        <Title>
                            Projects
                        </Title>
                    </TitleBox>

                    <Column1>
                        
                        <Text>
                            Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos 
                            et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata 
                            sanctus est Lorem ipsum dolor sit amet. 
                            Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos 
                            et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata 
                            amet. Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos 
                            et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata
                        </Text>
                        <img src={Docuware} height='100px' width='200px' alt='docuware1'/>
                        
                    </Column1>
                </Left>

                <Column2>
                    <div>
                        <Text>
                            Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos 
                            et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata 
                            sanctus est Lorem ipsum dolor sit amet. 
                            Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos 
                            et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata 
                            amet. Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos 
                            et accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata
                        </Text>
                        <img src={Digi} height='200px' width='300px' alt='docuware2'/>
                    </div>
                </Column2>

            </Box>    


        </Pattern>
            
        
            
        
    )
}

export default Projects
