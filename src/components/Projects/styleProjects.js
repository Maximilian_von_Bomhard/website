import styled from 'styled-components'
import back from '../../images/Hintergrund-Pattern-Projects.svg'


export const Pattern = styled.div`
    background-image: url(${back});
    width: 100%;
    min-height: 1600px;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: top;
    clip-path: polygon(0 15%, 100% 0%, 100% 79%, 0 100%);
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;

    @media screen and (max-width: 1400px){
        display: flex;
        background-image: none;
        background-color: #292929;
        clip-path:  polygon(0 10%, 100% 0%, 100% 90%, 0 100%);
        height: fit-content;
        
    }
`


export const Box= styled.div`
    //background: yellow;
    width: 100%;
    height: 800px;
    z-index: 2;
    display: flex;
    justify-content: space-around;
    align-items: flex-start;
    flex-wrap: wrap;

    @media screen and (max-width: 1400px){
        flex-direction: row;
        justify-content: space-evenly;
        height: 2000px;
    }
    

`

export const Left = styled.div`
    display: flex;
    width: 50%;
    padding-right:40px;
    
    @media screen and (max-width: 1400px){
        width: 80%;
        flex-direction: column;
        justify-content: center;
        margin-top: 200px;
        
    }
    
    

`

export const Column1 = styled.div`
    
    width: 50%;
    color: white;
    line-height: 30px;
    display: flex;
    flex-direction: column;
    justify-content: center;

    @media screen and (max-width: 1400px){
        width: 100%;
        justify-content: center;
        align-items: center;
        
    }
`



export const Column2 = styled.div`
    width: 50%;
    display: flex;
    justify-content: center;
    margin-top: -80px;
    

    @media screen and (max-width: 1400px){
        height: fit-content;
        color: white;
        width: 100%;
        background-color: #00D4FF;
       
    }
   
`

export const TitleBox = styled.div`
    
    display:flex;
    justify-content: flex-end;

    @media screen and (max-width: 1400px){
        justify-content: center;
        margin-top: 30px; 
        margin-bottom: 20px;
    }
    
`



export const Title = styled.h1`
    transform: rotate(180deg);
    writing-mode: vertical-rl;
    color: white;
    font-family: Helvetica;
    font-weight: bold;
    text-align: left;
    font-size: 90px;

    @media screen and (max-width: 1400px){
        transform: none;
        writing-mode: horizontal-tb;
    }
`

export const Text = styled.div`
    font-family: Calibri;
    font-size: 23px;
    line-height: 40px;

    min-width: 300px;
    max-width: 500px;
    
    /* @media screen and (max-width: 1400px){
       text-align: center
    } */
`




