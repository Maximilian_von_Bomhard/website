import React from 'react'
import { Box, Container, Input, FormBox, Map, Label, Button, MessageBox, ButtonBox, Textarea, Box2, LabelBox } from './styleContact'
import Maps from './Map.js'
import emailjs from 'emailjs-com'
function Contact() {

    const sendEmail = (e) => {

        e.preventDefault();

        emailjs.sendForm('service_aanqune', 'template_ryfof6o', e.target, 'user_vuXDETTShbaEEu6Z8xwd9')
          .then((result) => {
              console.log(result.text);
          }, (error) => {
              console.log(error.text);
          });
          e.target.reset()
    }
    return (
        <Container id="contact">
            <Box>
            
                <FormBox>
                    <form onSubmit={sendEmail}>
                        <Box2>
                            <LabelBox>
                                
                                <Label>Name</Label>
                                
                                <Input name="name"/>
                            </LabelBox>

                            <LabelBox>
                                
                                <Label>E-Mail</Label>
                                
                                <Input name="email" />
                            </LabelBox>

                            <MessageBox>
                                <ButtonBox>
                                    <Label>Message</Label>
                                    <Button type='submit'>Send</Button>
                                </ButtonBox>
                                <Textarea name="message"  cols="58" rows="11"></Textarea>
                                
                            </MessageBox>
                        </Box2>
                    </form>
                </FormBox>
                <Map>
                    <Maps lat=' 11.36' lng=' 48.13'/>
                </Map>


            </Box>
        </Container>
    )
}

export default Contact
