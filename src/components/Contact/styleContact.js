import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: center;
    padding-top: 70px;
    padding-bottom: 70px;
   
    
    
`

export const Box = styled.div`
    display:flex;
    width: 80%;
    height: fit-content;
    justify-content: space-evenly;
    /* background: yellow; */
    margin-top: 50px;
    flex-wrap: wrap;
   


`

export const FormBox = styled.div`
    width: 60%;
    min-width: 800px;

`

export const Label = styled.div`
    color: white;
    background: #292929;
    margin: 10px;
    text-align: center;
    width: 110px;
    font-size: 25px;
    font-family: Helvetica;
`

export const Button = styled.button`
    width: 110px;
    margin: 10px;
    background: #FF0084;
    color: white;
    font-size: 25px;
    border: none;
    font-family: Helvetica;
    cursor: pointer;

    &:hover{
        background: #b4005d ;
    }
`


export const Input = styled.input`
    width: 70%;
    height: 100%;  
    border: none;
    background-color: #f2f2f2; 
    font-size: 25px; 
    border-bottom: 2px solid #626262;
`


export const Map = styled.div`
    margin: 10px;
    
`


export const MessageBox = styled.div`
    display: flex;
    padding-top: 10px;
`

export const ButtonBox = styled.div`
    display: flex;
    flex-direction: column;
`

export const Textarea = styled.textarea`
    width: 70%;
    border: none;
    background-color: #f2f2f2; 
    font-size: 20px;
    resize: vertical;
    max-height: 400px;
    border-bottom: 2px solid #626262;
    

`
export const Box2 = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
   
`

export const LabelBox = styled.div`
    display: flex;
    align-items: center;
`