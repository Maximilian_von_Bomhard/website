import React from 'react'
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';


const containerStyle = {
  width: '450px',
  height: '450px'
};

function Maps() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: "AIzaSyBcuECp26DLifEUxBXLuaPbwBlnktP3SCY"
  })

 

  return isLoaded ? (
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={{lat: 48.129999,lng: 11.366525}}
        zoom={17}
      >
          
        <Marker position={{lat: 48.129999, lng: 11.366525}} />
          
      </GoogleMap>
  ) : <></>
}

export default React.memo(Maps)


