import styled from "styled-components";



export const Div = styled.div`
    display: flex;
    justify-content: center;
    padding-bottom: 150px;
    min-height: 750px;
    //max-height:900px;
    //clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 100%);
`
export const Container = styled.div`
    display: flex;
    width: auto;
    align-items: flex-end;
    justify-content: center;
    margin-left: 80px;
    //padding-bottom: 10px;
    //background-color: yellow;
    //padding-top: 100px;
     
`

export const Textbox = styled.div`
    display: flex;
    justify-content: flex-end;
    width: 80%;
    
    

`

export const Text = styled.div`
    width: 30%;
    text-align: left;
    margin: 30px;
    font-family: Calibri;
    font-size: 23px;
    line-height: 40px;
    
`


export const Titlebox = styled.div`
    display: flex;
    
    margin-bottom: 30px;
    
    //max-height: 50px;
    /* width: 100%; */
    
`

export const Title = styled.h1`
    font-size: 90px;
    transform: rotate(180deg);
    text-align: left;
    writing-mode: vertical-rl;
    margin-right: 0px;
    font-family: Helvetica;
    font-weight: bold;
    //background: yellow;
`