import React, { useEffect } from 'react'
import { Container, Textbox, Titlebox, Text, Div, Title } from './styleAbout'
import GlobalFonts from '../../Fonts/fonts';
import Aos from 'aos'
import 'aos/dist/aos.css'

function About() {

    useEffect(() => {
        Aos.init({duration: 1500});
    }, []);

    return (
        <Div id="about" >
            <GlobalFonts/>
               
            <Container>
               

                <Textbox data-aos='fade-up'>
                    
                    <Titlebox>
                        <Title>
                            About Titanom
                        </Title>
                    </Titlebox>
                    
                    <Text>
                        Lorem ipsum dolor sit amet, consetetur 
                        sadipscing elitr, sed diam nonumy eirmod 
                        tempor invidunt ut labore et dolore magna 
                        aliquyam erat, sed diam voluptua. At vero eos et 
                        accusam et justo duo dolores et ea rebum. Stet 
                        clita kasd gubergren, no sea takimata sanctus est 
                        Lorem ipsum dolor sit amet. 
                        <p>
                        Lorem ipsum dolor sit amet, consetetur 
                        sadipscing elitr, sed diam nonumy eirmod 
                        tempor invidunt ut labore et dolore magna
                        aliquyam erat, sed diam voluptua. At vero eos et 
                        accusam et justo duo dolores et ea rebum. 
                        </p>
                        Stet clita kasd gubergren, no sea takimata 
                        sanctus est Lorem ipsum dolor sit amet. Lorem 
                        ipsum dolor sit amet, consetetur sadipscing elitr, 
                        sed diam nonumy eirmod tempor invidunt ut 
                        labore et dolore magna aliquyam erat, sed diam 
                        voluptua. At vero eos et accusam et justo duo 
                        dolores et ea rebum. Stet clita kasd gubergren, 
                        no sea takimata sanctus est Lorem ipsum dolor 
                        sit amet. Lorem ipsum dolor sit amet, consetetur 
                        sadipscing elitr, sed diam nonumy eirmod 
                        tempor invidunt ut labore et dolore magna 
                        aliquyam erat, sed diam voluptua. At vero eos et 
                        accusam et justo duo dolores et ea rebum. Stet 
                        clita kasd gubergren, no sea takimata sanctus est
                    </Text>

                    <Text>
                            Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna 
                            aliquyam erat, sed diam voluptua. At vero eos et 
                            accusam et justo duo dolores et ea rebum. Stet 
                            clita kasd gubergren, no sea takimata sanctus est 
                            Lorem ipsum dolor sit amet. 
                            Lorem ipsum dolor sit amet, consetetur 
                            sadipscing elitr, sed diam nonumy eirmod 
                            tempor invidunt ut labore et dolore magna
                            aliquyam erat, sed diam voluptua. At vero eos et 
                            accusam et justo duo dolores et ea rebum. 
                            Stet clita kasd gubergren, no sea takimata 
                            sanctus est Lorem ipsum dolor sit amet. Lorem 
                            ipsum dolor sit amet, consetetur sadipscing elitr, 
                            sed diam nonumy eirmod tempor invidunt ut 
                            labore et dolore magna aliquyam erat, sed diam 
                            voluptua. At vero eos et accusam et justo duo 
                            dolores et ea rebum. Stet clita kasd gubergren, 
                            no sea takimata sanctus est Lorem ipsum dolor 
                            sit amet. 
                            <p>
                                Lorem ipsum dolor sit amet, consetetur 
                                sadipscing elitr, sed diam nonumy eirmod 
                                tempor invidunt ut labore et dolore magna 
                                aliquyam erat, sed diam voluptua. At vero eos et 
                                accusam et justo duo dolores et ea rebum. Stet 
                                clita kasd gubergren, no sea takimata sanctus est
                                
                            </p>
                    </Text>
                </Textbox>
            </Container>
        </Div>
    )
}

export default About

