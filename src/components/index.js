export { default as Navbar } from "./Navigation/Navbar";
export { default as Home } from "./Home/Home";
export { default as About } from "./About/About";
export { default as Team } from "./Team/Team";
export { default as Projects } from "./Projects/Projects";
export { default as Contact } from "./Contact/Contact";
export { default as Footer } from "./Footer/Footer";

export {default as Navigation} from './Navigation/Navigation'