import './App.css';
import { About, Home, Team, Projects, Contact, Footer, Navigation } from './components';

function App() {
  return (
    <div className="App">
      <Navigation/>

      <div className="light">
        
        <Home/>
        
        <div className="Watersign">
          <About/>

          <Team />
        </div>

        <Projects/>
        
        <div className="MailUs">
          <Contact/>

          <Footer/>
        </div>
      </div>
    </div>
  );
}

export default App;
